#include <iomanip>
#include <iostream>
#include <fstream>
#include <limits>
#include "c45Wrapper.cpp"

void menu();
void LearnDecisionTree(c45Wrapper*);
void TestAccuracy(c45Wrapper*);
void ApplyToNewCases(c45Wrapper*);
void LoadTreeModel(c45Wrapper*);

int main(int argc, char* argv[])
{
    menu();
    return 0;
}

void menu()
{

    const int QUIT = 5;
    int choice = -1, subChoice = -1;

    c45Wrapper* wrapper = new c45Wrapper();

    while(choice != QUIT)
    {
        std::cout << "1. Learn a decision tree and save the tree." << std::endl;
        std::cout << "2. Testing accuracy of the decision tree." << std::endl;
        std::cout << "3. Applying the decision tree to new cases." << std::endl;
        std::cout << "4. Load a tree model and apply to new cases interactively as in menu 3." << std::endl;
        std::cout << "5. Quit." << std::endl;

        std::cout << "Enter your choice: ";
        std::cin >> choice;

        if(std::cin.fail())
        {
            std::cout << std::endl << "Input is not a number." << std::endl << std::endl;
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            continue;
        }

        
        switch(choice)
        {
            case 1:
                LearnDecisionTree(wrapper);
                break;
            case 2:
                TestAccuracy(wrapper);
                break;
            case 3:
                ApplyToNewCases(wrapper);
                break;
            case 4:
                LoadTreeModel(wrapper);
                break;
            case QUIT:
                break;

            default:
                std::cout << "Invalid menu choice." << std::endl;
                break;
        }

        std::cout << std::endl;
    }

    
}

void LearnDecisionTree(c45Wrapper* wrapper)
{
    wrapper->GenerateTree();
}

void TestAccuracy(c45Wrapper* wrapper)
{
    wrapper->GetTestingData();
}

void ApplyToNewCases(c45Wrapper* wrapper)
{
    const int SUB_QUIT = 3;
    int subChoice = -1;
    while(subChoice != SUB_QUIT)
    {
        std::cout << std::endl;
        std::cout << "The submenu of this item includes:" << std::endl;
        std::cout << "    3.1 Enter a new case interactively." << std::endl;
        std::cout << "    3.2 Print tree." << std::endl;
        std::cout << "    3.3 Go back to main menu." << std::endl;

        std::cout << "Enter your choice: ";
        std::cin >> subChoice;
        std::cout << std::endl;
        switch(subChoice)
        {
            case 1:
                wrapper->TestCase();
                break;
            case 2:
                wrapper->PrintTree();
                break;
            case 3:
                break;

            default:
                std::cout << "Invalid menu choice." << std::endl;
                break;
        }
    }
}

void LoadTreeModel(c45Wrapper* wrapper)
{
    wrapper->GetFileNamesWithTree();
    wrapper->LoadTreeFromFile();
}