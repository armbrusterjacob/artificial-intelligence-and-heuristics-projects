#pragma once

#include <cstring>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <stack>
// Tree library from
// http://tree.phi-sci.com/index.html
#include "tree.hh"

const std::string TEMP_FILENAME_STEM = "temp_files/temporary"; 
const std::string DATA_DIRECTORY = "files/";

struct ConfusionCount
{
    int positives = 0;
    int negatives = 0;
};

struct ConditionVariable
{
    std::string name;
    std::vector<std::string> values;
};

struct MetaData
{
    std::vector<std::string> classes;
    std::vector<ConditionVariable*> conditions;
};

enum nodeType{
    equal,
    gtr,
    les,
    gtreq,
    leseq,
    is,
};
struct AttNode
{
    nodeType type;
    std::string attribute;
    int attIndex;
    std::string value = "-";

    //c4.5 tree members
    short NodeType;
    ClassNo Leaf;
    ItemCount Items;
    std::vector<ItemCount> ClassDist;
    ItemCount Errors;
    Attribute Tested;
    short Forks;
    float Cut;
    float Lower;
    float Upper;


    std::string to_string()
    {
        std::string temp = attribute + " ";
        temp += type_to_string() + " ";
        temp += value;

        return temp;
    }

    std::string type_to_string()
    {
        switch(type)
        {
            case nodeType::equal:
                return "=";
                break;

            case nodeType::gtr:
                return ">";
                break;

            case nodeType::les:
                return "<";
                break;

            case nodeType::gtreq:
                return ">=";
                break;

            case nodeType::leseq:
                return "<=";
                break;

            case nodeType::is:
                return ":";
        }
              
    }

    bool test_from_type(float lhs, float rhs)
    {

        switch(type)
        {
            case nodeType::equal:
                if(lhs == rhs)
                {
                   return true;
                }
                break;

            case nodeType::gtr:
                if(lhs > rhs)
                {
                   return true;
                }
                break;

            case nodeType::les:
                if(lhs < rhs)
                {
                   return true;
                }
                break;

            case nodeType::gtreq:
                if(lhs >= rhs)
                {
                   return true;
                }
                break;

            case nodeType::leseq:
                if(lhs <= rhs)
                {
                   return true;
                }
                break;
        }

        return false;
    }
};

class c45Wrapper
{
    std::string fileStem;
    std::string metadataFilename;
    std::string dataFilename;
    std::string treeFilename;
    std::string testingFilename;

    tree<AttNode*> decisionTree;
    tree<AttNode*>::iterator root = nullptr;

    std::fstream tfile;
    MetaData *metadata;

    public:
    
    c45Wrapper()
    {
        return;
    }

    void CopyFile(std::string srcName, std::string dstName);
    void removeTempFiles();
    void GetFilenames();
    void GetFileNamesWithTree();
    MetaData* parseMetaData();
    void PrintMetaData();
    void GenerateTree();
    void GetTestingData();
    void TestCase();
    void PrintTree();
    void LoadTreeFromFile();
    void InTree(tree<AttNode*>::iterator branch, int forkIndex);
    void parseDecisionTree();
    tree<AttNode*>::iterator parseDecisionNode(std::string str, tree<AttNode*>::iterator current, int depth);

};