Class: Artifical Inteligence and Heuristic Programming
Name: Jacob Armbruster

In order to use this program you need to use cmake inside the build folder as shown:

cmake ..
make

This will create c45Wrapper which can be then be run with "./c45Wrapper" in the build folder.

This program utilizes c4.5 and consult which are found in the program folder inside build.
It also requires that any file related to the program be placed in the "files/" directory
in the "build/" directory. This includes files with the following extenstions:

.names
.data
.tree
.test
.unpruned
.rules

This project comes crx files for .names, .data, .tree, .test. All of these files are datasets that
c4.5 provides as examples. There are also duplicated files with the filestem "other" made to test
the funcitonality for indivudally selecting filestems. The only of those which is not a duplicate is
other.test which features different data to show a different confusion matrix for menu item 2. 

This project utilizes tree.hh which can be found at: http://tree.phi-sci.com/index.html