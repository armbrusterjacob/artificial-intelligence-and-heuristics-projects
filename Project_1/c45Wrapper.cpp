#pragma once

#include <cstring>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <stack>
#include <map>
#include <stdio.h>
// Tree library from
// http://tree.phi-sci.com/index.html
#include "tree.hh"
#include "types.i"
#include "c45Wrapper.hpp"



// Menu Item 1
void c45Wrapper::GenerateTree()
{
    GetFileNamesWithTree();

    std::string input = "./program/c4.5 -f " + fileStem + "2> /dev/null";
    system(input.c_str());

    
    // if there already is a decision tree loaded then clear it
    decisionTree.clear();

    // load the decision tree
    LoadTreeFromFile();

    if(fileStem == TEMP_FILENAME_STEM)
    {
        CopyFile(TEMP_FILENAME_STEM + ".tree", treeFilename);
    }

    // Remove temp files
    if(fileStem == TEMP_FILENAME_STEM)
    {
        removeTempFiles();
    }
    
    std::cout << std::endl;
    std::cout << "Done." << std::endl;
}

// Menu Item 2

void c45Wrapper::GetTestingData()
{
    // Tree needs to be loaded first
    if(decisionTree.empty())
    {
        std::cout << "The tree hasn't been loaded yet. Use the menu's option 1 or 4 to load it." << std::endl;
        return;
    }
    metadata = parseMetaData();

    // Open file from user input
    std::string testFilename = "";
    std::cout << "Filestem for testing file?: ";
    std::cin >> testFilename;
    testFilename = DATA_DIRECTORY  + testFilename + ".test";
    std::ifstream file;
    file.open(testFilename.c_str(), std::ios_base::app);
    if(file.fail())
    {
        std::cout << testFilename << " not found." << std::endl;
        return;
    }

    // Intialize confusion matrix
    int metadataSize = metadata->classes.size();
    int confusionMatrix[metadataSize][metadataSize];
    for(size_t i = 0; i < metadataSize; i++)
    {
        for(size_t j = 0; j < metadataSize; j++)
        {
            confusionMatrix[i][j] = 0;
        }
    }
    
    // Test each case in data file
    std::string str;
    int lines = 0;
    while(getline(file,str))
    {
        lines++;
        // Get the line's decision
        int decisionStart = str.find_last_of(",") + 1;
        int terminatingDot = str.find_last_of(".");
        std::string testDecision = str.substr(decisionStart, terminatingDot - decisionStart);
        std::string foundDecision;

        // Set up tree iterator
        tree<AttNode*>::iterator it = decisionTree.begin_breadth_first();
        tree_node_<AttNode*>* nd = it.node;

        // Get first node
        while(nd->data == nullptr || nd->data->type == nodeType::is)
        {
            std::cout.flush();
            nd = nd->first_child;
        }

        bool madeDecision = false;

        // Test each depth level
        while(!madeDecision)
        {
            bool isContinuous = false;
            bool madeMatch = false;

            std::vector<tree_node_<AttNode*>*> nodes;
            while(nd->next_sibling != nullptr)
            {
                nodes.push_back(nd);
                nd = nd->next_sibling;
            }
            nodes.push_back(nd);

            // Check if values are continuous
            if(nodes.at(0)->data->value == nodes.at(1)->data->value)
            {
                isContinuous = true;
            }

            // Get Conditon Variable
            int start = 0;
            int end = str.find_first_of(",", start);
            for(size_t i = 0; i < nd->data->attIndex; i++)
            {
                start = end + 1; // skip delimiter   
                end = str.find_first_of(",", start);
            }
            std::string input = str.substr(start, end - start);

            // Try to find a match
            if(!isContinuous)
            {
                for(tree_node_<AttNode*>* a : nodes)
                {
                    if(input == a->data->value)
                    {
                        madeMatch = true;
                        if(a->first_child->data->attribute == "decision")
                        {
                            // Test whether the correct decison was chosen
                            foundDecision = a->first_child->data->value;
                            madeDecision = true;
                        }else{
                            nd = a->first_child;
                        }
                    }
                }
            }else
            {
                float lhs = atof(input.c_str());
                for(tree_node_<AttNode*>* a : nodes)
                {
                    float rhs = atof(a->data->value.c_str());

                    if(a->data->test_from_type(lhs, rhs))
                    {
                        madeMatch = true;
                        if(a->first_child->data->attribute == "decision")
                        {
                            // Test whether the correct decison was chosen
                            foundDecision = a->first_child->data->value;
                            madeDecision = true;
                        }else{
                            nd = a->first_child;
                        }
                    }
                }
            }
            if(!madeMatch)
            {
                std::cout << "Input doesn't match defined value. [" << input << ":" << nd->data->attribute << "]" << std::endl << std::endl;
                std::cout << "Possible file mismatch" << std::endl;
                return;
            }
        }
        int actualIndex, predictedIndex;
        for(size_t i = 0; i < metadata->classes.size(); i++)
        {
            std::string name = metadata->classes[i];
            if(foundDecision == name)
            {
                actualIndex = i;
            }
            if(testDecision == name)
            {
                predictedIndex = i;
            }
        }
        confusionMatrix[actualIndex][predictedIndex]++;
    }

    // Print the confusion matrix

    // Find class with max length
    int maxLength = 0;
    for(size_t i = 0; i < metadataSize; i++)
    {
        int length = metadata->classes[i].length();
        if(maxLength < length)
            maxLength = length;
    }

    std::cout << std::endl << "Confusion Matrix:" << std::endl << std::endl;
    std::cout << std::string(maxLength + 1, ' ');
    for(size_t i = 0; i < metadataSize; i++)
    {
        std::cout << metadata->classes[i] << " ";
    }
    std::cout << std::endl;
    std::cout << std::left;
    for(size_t i = 0; i < metadataSize; i++)
    {
        std::cout << metadata->classes[i] << std::string(maxLength - metadata->classes[i].length() + 1, ' ');
        for(size_t j = 0; j < metadataSize; j++)
        {
            std::cout << std::setw(metadata->classes[j].length());
            std::cout << confusionMatrix[i][j] << " ";
        }
        std::cout << std::endl;
    }

    file.close();

    return;
}

// Menu Item 3.1
void c45Wrapper::TestCase()
{
    if(decisionTree.empty())
    {
        std::cout << "The tree hasn't been loaded yet. Use the menu's option 1 or 4 to load it." << std::endl;
        return;
    }

    // Set up tree iterator
    tree<AttNode*>::iterator it = decisionTree.begin_breadth_first();
    tree_node_<AttNode*>* nd = it.node;

    while(nd->data == nullptr || nd->data->type == nodeType::is)
    {
        
        std::cout.flush();
        nd = nd->first_child;
    }

    std::string input = " ";
    std::cout << "Testing a case: " << std::endl << std::endl;
    // Test each depth level
    bool madeDecision = false;
    while(!madeDecision)
    {
        bool isContinuous = false;

        // Push every node's value into a vector
        std::vector<tree_node_<AttNode*>*> nodes;
        while(nd->next_sibling != nullptr)
        {
            nodes.push_back(nd);
            nd = nd->next_sibling;
        }
        nodes.push_back(nd);

        // check if values are continuous
        if(nodes.at(0)->data->value == nodes.at(1)->data->value)
        {
            isContinuous = true;
        }

        // Check user input and continue if it's invalid
        bool madeMatch = false; 
        while(!madeMatch)
        {
            // output options
            std::cout << "possible inputs ( ";
            for(tree_node_<AttNode*>* a : nodes)
            {
                if(!isContinuous)
                {
                    std::cout << a->data->value << " ";
                }else
                {
                    std::cout << "'"  << a->data->type_to_string() << " " << a->data->value << "' ";
                }
            }
            std::cout << ")" << std::endl;
            std::cout << nd->data->attribute << ": "; 

            // get user input
            std::cin >> input;
            std::cout << std::endl;

            if(!isContinuous)
            {
                for(tree_node_<AttNode*>* a : nodes)
                {
                    if(input == a->data->value)
                    {
                        madeMatch = true;
                        // If we've found our decision value
                        if(a->first_child->data->attribute == "decision")
                        {
                            std::cout << "The decision value is: " << a->first_child->data->value << std::endl;
                            madeDecision = true;
                            break;
                        }else{
                            nd = a->first_child;
                        }
                    }
                }

                if(!madeMatch)
                {
                    std::cout << "Input doesn't match defined value." << std::endl << std::endl;
                }
            }else
            {
                bool madeMatch = false;
                float lhs = atof(input.c_str());
                for(tree_node_<AttNode*>* a : nodes)
                {
                    float rhs = atof(a->data->value.c_str());

                    madeMatch = a->data->test_from_type(lhs, rhs);
                    if(madeMatch)
                    {
                        // If we've found our decision value
                        if(a->first_child->data->attribute == "decision")
                        {
                            std::cout << "Decision is: " << a->first_child->data->value << std::endl;
                            madeDecision = true;
                        }else{
                            nd = a->first_child;
                        }
                        break;
                    }
                }
                break;
            }
        }
    }
}

// Menu Item 3.2
void c45Wrapper::PrintTree()
{
    if(decisionTree.empty())
    {
        std::cout << "The tree hasn't been loaded yet. Use the menu's option 1 or 4 to load it." << std::endl;
        return;
    }

    for(tree<AttNode*>::iterator it = root.begin(); it != root.end(); it++)
    {
        if(decisionTree.depth(it.node) > 1)
        {
            for(size_t i = 0; i < decisionTree.depth(it.node) - 2; i++) 
            {
                std::cout << " | ";
            }
            
            std::cout << it.node->data->to_string();
            // std::cout << " " << it.node->data->NodeType;
            std::cout << std::endl;
        }
    }
}

// Menu Item 4
void c45Wrapper::LoadTreeFromFile()
{
    if(fileStem == TEMP_FILENAME_STEM)
    {
        std::string temp = TEMP_FILENAME_STEM + ".tree";
        tfile.open(temp.c_str(), std::ios::in | std::ios::binary);
    }else{
        tfile.open(treeFilename.c_str(), std::ios::in | std::ios::binary);
    }

    metadata = parseMetaData();
    // PrintMetaData();

    if(tfile.fail())
    {
        std::cout << "file not found" << std::endl;
        return;
    }

    // Remove any previous tree
    decisionTree.clear();

    // Initilize new tree
    tree<AttNode*>::iterator top;
    AttNode* head = nullptr;
    top = decisionTree.begin();
    root = decisionTree.insert(top, head);

    // skip 1st character
    tfile.ignore();

    InTree(root, 0);

    std::cout << "Tree loaded." << std::endl;
    std::cout << std::endl;

    tfile.close();

    // Remove temp files
    if(fileStem == TEMP_FILENAME_STEM)
    {
        removeTempFiles();
    }

    return;
}


// Copy file from srcName into DstName
void c45Wrapper::CopyFile(std::string srcName, std::string dstName)
{
    std::ifstream src(srcName, std::ios::in | std::ios::binary);
    std::ofstream dst(dstName, std::ios::out | std::ios::binary);
    dst << src.rdbuf();

    src.close();
    dst.close();

    return;
}

// Removes tempoary files created with the GetFilenames methods
void c45Wrapper::removeTempFiles()
{
    std::string input;
    input = TEMP_FILENAME_STEM + ".names ";
    remove(input.c_str());
    input = TEMP_FILENAME_STEM + ".data ";
    remove(input.c_str());
    input = TEMP_FILENAME_STEM + ".tree ";
    remove(input.c_str());
    input = TEMP_FILENAME_STEM + ".unpruned ";
    remove(input.c_str());
    input = TEMP_FILENAME_STEM + ".test ";
    remove(input.c_str());
}

// Prompt the user if they wish to use a simple filestem that unifys the naming scheme between
// files or if they want to select the stem for each file individually.
void c45Wrapper::GetFilenames()
{
    while(true)
    {
        std::string temp;
        std::cout << "Get files through one filestem or each indivdually? ['f' or 'i']: ";
        std::cin >> temp;
        if(temp == "i")
        {
            std::cout << "(if applicable) Filestem for metadata?: ";
            std::cin >> metadataFilename;
            metadataFilename = DATA_DIRECTORY + metadataFilename + ".names";

            std::cout << "(if applicable) Filestem for data?: ";
            std::cin >> dataFilename;
            dataFilename = DATA_DIRECTORY + dataFilename + ".data";

            fileStem = TEMP_FILENAME_STEM;

            CopyFile(metadataFilename, fileStem + ".names");
            CopyFile(dataFilename, fileStem + ".data");
            break;

        }else if(temp == "f")
        {
            std::cout << "What is the filestem?: ";
            std::cin >> fileStem;
            fileStem = DATA_DIRECTORY + fileStem;
            break;
        }else
        {
            std::cout << "Invalid input (not 'f' or 'i')." << std::endl;
        }
        std::cout << std::endl;
    }
}

// GetFilenames but wth an additional indvidual prompt for .tree files
void c45Wrapper::GetFileNamesWithTree()
{
    GetFilenames();
    if(fileStem == TEMP_FILENAME_STEM)
    {
        std::cout << "(if applicable) Filestem for new tree file?: ";
        std::cin >> treeFilename;
        treeFilename = DATA_DIRECTORY + treeFilename + ".tree";
        CopyFile(treeFilename, fileStem + ".tree");
    }
    else
    {
        treeFilename = fileStem + ".tree";
    }
}

// Parse the metadata off of fileStem.names
MetaData* c45Wrapper::parseMetaData()
{
    if(metadata)
    {
        delete metadata;
    }

    std::string metaFilename;
    if(fileStem == TEMP_FILENAME_STEM)
    {
        metaFilename = TEMP_FILENAME_STEM + ".names";
    }else{
        metaFilename = fileStem + ".names";
    }

    std::string metaDataString;
    std::ifstream file;
    file.open(metaFilename.c_str(), std::ios_base::app);

    if(file.fail())
    {
        std::cout << metaFilename << " not found." << std::endl;
        exit(0);
    }

    MetaData* names = new MetaData();
    
    std::string str;

    // Skip all comments and blank lines
    while(getline(file, str))
    {
        std::string temp = str;
        temp.erase(remove_if(temp.begin(), temp.end(), isspace), temp.end());
        if(temp[0] != '|' && temp.length() != 0)
        {
            break;
        }
    }

    // Parse Classes

    // Remove trailing spaces and any comments
    int start = 0;
    int commentPos = str.find_first_of('|');
    if(commentPos != std::string::npos)
    {
        str = str.substr(start, commentPos);
    }

    int lastChar = str.find_last_not_of(",. \t\f\v\n\r");
    if(lastChar == str.length() - 1)
    {
        str = str + '.';
    }
    else if(str[lastChar + 1] != '.')
    {
        str[lastChar + 1] = '.';
    }

    int end = str.find_first_of(",.", start);
    while(end != std::string::npos)
    {
        // remove leading spaces
        int nonWhiteStart = str.find_first_not_of(" \t\f\v\n\r", start);
        names->classes.push_back(str.substr(nonWhiteStart, end - nonWhiteStart));
        start = end + 1; // 1 == size of delimiter
        end = str.find_first_of(".,", start);
    }

    // Skip all comments and blank lines
    while(getline(file, str))
    {
        std::string temp = str;
        temp.erase(remove_if(temp.begin(), temp.end(), isspace), temp.end());
        if(temp[0] != '|' && temp.length() != 0)
        {
            break;
        }
    }
    
    // Parse Conditions
    do
    {
        ConditionVariable* var = new ConditionVariable();

        // remove  trailing spaces and any comments
        int start = 0;
        int commentPos = str.find_first_of('|');
        if(commentPos != std::string::npos)
        {
            str = str.substr(start, commentPos);
        }

        int lastChar = str.find_last_not_of(",. \t\f\v\n\r");
        if(lastChar == str.length() - 1)
        {
            str = str + '.';
        }
        else if(str[lastChar + 1] != '.')
        {
            str[lastChar + 1] = '.';
        }
        
        // get attribute name
        int end = str.find_first_of(":");
        var->name = str.substr(start, end);

        // Parse condtion variables
        start = end + 1; // skip the ':'
        end = str.find_first_of(",.", start);
        while(end != std::string::npos)
        {
            // remove leading spaces
            int nonWhiteStart = str.find_first_not_of(" \t\f\v\n\r", start);
            var->values.push_back(str.substr(nonWhiteStart, end - nonWhiteStart));
            start = end + 1; // skip delimiter
            end = str.find_first_of(".,", start);
        }

        names->conditions.push_back(var);
    }while(getline(file, str));

    file.close();

    return names;
}

// Print Metadata

void c45Wrapper::PrintMetaData()
{
    std::cout << "Printing metadata..." << std::endl;
    std::cout << "  classes: ";
    for(size_t i = 0; i < metadata->classes.size(); i++)
    {
        std::cout << '\'' << metadata->classes.at(i) << '\'' << ", ";
    }
    std::cout << std::endl;

    for(size_t i = 0; i < metadata->conditions.size(); i++)
    {
        std::cout << metadata->conditions[i]->name << ": ";
        std::vector<std::string>* curr_var = &metadata->conditions[i]->values;
        for(size_t j = 0; j < curr_var->size(); j++)
        {
            std::cout << '\'' << curr_var->at(j) << '\'' << ", ";
        }
        std::cout << std::endl;
    }
    std::cout << "Done. " << std::endl;
}

// A recursive helper function for LoadTreeFromFile()
void c45Wrapper::InTree(tree<AttNode*>::iterator branch, int forkIndex)
{
    // auto node = branch.node->data;
    AttNode* parent = branch.node->data;
    AttNode* node = new AttNode();
    tree<AttNode*>::iterator current = decisionTree.append_child(branch, node);
    
    tfile.readsome(reinterpret_cast<char *>(&node->NodeType), sizeof(short));
    tfile.readsome(reinterpret_cast<char *>(&node->Leaf), sizeof(short));
    tfile.readsome(reinterpret_cast<char *>(&node->Items), sizeof(ItemCount));
    tfile.readsome(reinterpret_cast<char *>(&node->Errors), sizeof(ItemCount));

    // std::cout << "START" << std::endl;
    // std::cout << "  NodeType: " << node->NodeType << std::endl;
    // std::cout << "  Leaf: " << node->Leaf << std::endl;
    // std::cout << "  Items: " << node->Items << std::endl;
    // std::cout << "  Errors: " << node->Errors << std::endl;
    // std::cout << "  metadataclasses: " << metadata->classes.size() << std::endl;
    
    // MaxClass refers to the size of deision variables so we use the metadata's class size
    
    for(size_t i = 0; i < metadata->classes.size(); i++)
    {
        ItemCount temp;
        tfile.readsome(reinterpret_cast<char *>(&temp), sizeof(ItemCount));
        node->ClassDist.push_back(temp);
    }
    // tfile.readsome(reinterpret_cast<char *>(&node->ClassDist), metadata->classes.size() * sizeof(ItemCount));

    if(parent)
    {
    //     std::cout << "  Fork Index" << ": " << forkIndex << std::endl;
    //     std::cout << "  parent's tested" << ": " << parent->Tested << std::endl;
        node->attribute = metadata->conditions.at(parent->Tested)->name;
        node->attIndex = parent->Tested;

        // if continutous
        if(metadata->conditions.at(parent->Tested)->values.size() == 1)
        {
            // first continuous value is always <=, second is >
            if(forkIndex == 0)
            {
                node->type = nodeType::leseq;
            }
            else
            {
                node->type = nodeType::gtr;
            }

            node->value = std::to_string(parent->Cut);
        }
        else
        {
            node->value = metadata->conditions.at(parent->Tested)->values.at(forkIndex);
        }
    }
    else
    {
        node->attribute = "root";
        node->type = nodeType::is;
        node->value = "";
    }

    if(node->NodeType)
    {
        tfile.readsome(reinterpret_cast<char *>(&node->Tested), sizeof(Attribute));
        tfile.readsome(reinterpret_cast<char *>(&node->Forks), sizeof(short));

        // std::cout << "START" << std::endl;
        // std::cout << "  ClassDist: {";
        // for(size_t i = 0; i < metadata->classes.size(); i++)
        // {
        //     std::cout << node->ClassDist.at(i) << ", ";
        // }
        // std::cout << "}" << std::endl;
        
        // std::cout << "  Tested" << ": " << node->Tested << std::endl;
        // std::cout << "  Condition List Size" << ": " << metadata->conditions.at(node->Tested)->values.size() << std::endl;

        // std::cout << "got here" << std::endl;
        switch(node->NodeType)
        {
            case BrDiscr:
                // simple case where a decison is made
                break;
            
            case ThreshContin:
                tfile.readsome(reinterpret_cast<char *>(&node->Cut), sizeof(float));
                tfile.readsome(reinterpret_cast<char *>(&node->Lower), sizeof(float));
                tfile.readsome(reinterpret_cast<char *>(&node->Upper), sizeof(float));
                
                // std::cout << "  Cut" << ": " << node->Cut << std::endl;
                // std::cout << "  Lower" << ": " << node->Lower << std::endl;
                // std::cout << "  Upper" << ": " << node->Upper << std::endl;

                break;
            
            case BrSubset:
                // currently unused
                break;
                
        }
        // std::cout << "  Forks" << ": " << node->Forks << std::endl;
        for(size_t i = 0; i < node->Forks; i++)
        {
            InTree(current, i);
        }
    } // if we have a leaf node
    else
    {
        if(parent)
        {
            node->attribute = metadata->conditions.at(parent->Tested)->name;

            AttNode* decision = new AttNode();
            decision->attribute = "decision";
            decision->type = nodeType::is;
            decision->value = metadata->classes.at(node->Leaf);
            decisionTree.append_child(current, decision);
        }
    }
}

/*

An older unused funciton that used to make a tree from the text output of the file

// parses a decision tree to fill out the object's decisionTree member
void c45Wrapper::parseDecisionTree()
{

    std::string filename = OUTPUTFILE;
    std::ifstream file;
    file.open(filename.c_str(), std::ios_base::app);

    // MetaData* metadata = parseMetaData();

    std::string str;
    while(getline(file, str))
    {
        if(str == "Decision Tree:")
        {
            getline(file, str); //advance a line
            break;
        }
    }

    // Now we're at the tree

    tree<AttNode*>::iterator top;
    std::stack<tree<AttNode*>::iterator> parentStack;

    AttNode* head = nullptr;
    top = decisionTree.begin();
    root = decisionTree.insert(top, head);
    parentStack.push(root);

    int prevDepth = 0;
    const int SIZE_DEPTH_OFFSET = 2;
    while(getline(file, str))
    {


        if(str.length() == 0)
        {
            break;
        }

        //remove whitespace
        str.erase(remove_if(str.begin(), str.end(), isspace), str.end());

        // count the '|''s which represent the depth of the current node
        int depth = 0;
        for(size_t i = 0; i < str.length(); i++)
        {
            if(str[i] != '|')
            {
                break;
            }
            else
            {
                depth++;
            }
        }
        
        if(depth == prevDepth && parentStack.size() > 1)
        {
            parentStack.pop();
        }

        // for every time depth is less than previous depth, go up the tree
        for(size_t i = 0; i < prevDepth - depth && prevDepth - depth > 0; i++)
        {
            parentStack.pop();
            if(depth == parentStack.size() - SIZE_DEPTH_OFFSET)
            {
                parentStack.pop();
            }
        }

        parentStack.push(parseDecisionNode(str, parentStack.top(), depth));

        prevDepth = depth;

        // std::cout << parentStack.top().node->data->to_string() << " depth: " << parentStack.top().node->data->depth;
        // std::cout << ", size: " << parentStack.size() - SIZE_DEPTH_OFFSET << std::endl;
    }

    file.close();

}

// helper method for parseDecisionTree()
tree<AttNode*>::iterator c45Wrapper::parseDecisionNode(std::string str, tree<AttNode*>::iterator current, int depth)
{
    // remove leading '|''s
    str = str.substr(depth);

    int previousBreak = 0;
    int wordLength = 0;
    bool isDecisionVariable = true;
    AttNode* node = new AttNode();
    for(size_t i = 0; i < str.length(); i++)
    {
        if(str[i] == '|')
        {
            continue;
        }

        if(str[i] == '=' || str[i] == '<' || str[i] == '>')
        {
            switch(str[i])
            {
                case '=':
                    node->type = nodeType::equal;
                    break;
                case '<':
                    if(str[i + 1] != '=')
                    {
                        node->type = nodeType::les;
                        i++;
                    }else
                    {
                        node->type = nodeType::leseq;
                        i++;
                    }
                    break;
                case '>':
                    if(str[i + 1] != '=')
                    {
                        node->type = nodeType::gtr;

                    }else
                    {
                        node->type = nodeType::gtreq;
                    }
                    break;
            }
            node->attribute = str.substr(previousBreak, wordLength);
            previousBreak = i + 1;
            wordLength = 0;
        }
        else
        {
            wordLength++;
        }

        if(str[i] == ':')
        {

            node->value = str.substr(previousBreak, wordLength - 1);
            node->depth = depth;
            previousBreak = i + 1;
            wordLength = 0;
            //if we're at the last character on the line at the colon
            if(i == str.length() - 1)
            {
                isDecisionVariable = false;
                return decisionTree.append_child(current, node);
                
            }
        }

        if(str[i] == '(')
        {
            // we're at a decision variable
            AttNode* decision = new AttNode();
            decision->attribute = "decision";
            decision->type = nodeType::is;
            decision->value = str.substr(previousBreak, wordLength - 1);
            decision->depth = depth + 1;

            current = decisionTree.append_child(current, node);
            decisionTree.append_child(current, decision);
            return current;
        }
    }
}

*/