#include <cstring>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <stack>
#include <map>
#include <stdio.h>
#include <random>
#include <algorithm>
#include "naiveBayes.hpp"

// Menu Item 1
void NaiveBayes::learnNaiveBayes()
{
    std::string metaFilename = DATA_DIRECTORY;
    std::string dataFilename = DATA_DIRECTORY;
    std::string str;
    std::cout << "Name for metadata file (ex: vote.names): ";
    std::cin >> str;
    metaFilename += str;
    std::cout << "Name for data file (ex: vote.data): ";
    std::cin >> str;
    dataFilename += str;

    parseMetaData(metaFilename);
    initialzeTableFromMetaData();
    parseDataFile(dataFilename);
    std::cout << "Done." << std::endl;
}

// Menu Item 2
void NaiveBayes::testAccuracy()
{
    if(table.empty())
    {
        std::cout << "You must have a table loaded to enter cases." << std::endl;
        std::cout << "Use menu item 1 or 5 to load a table in." << std::endl;
        return;
    }

    std::string tableFilename = DATA_DIRECTORY;
    std::string str;
    std::cout << "Name for test file (ex: vote.test): ";
    std::cin >> str;
    tableFilename += str;

    std::ifstream file;
    file.open(tableFilename.c_str(), std::ios_base::app);

    if(file.fail())
    {
        std::cout << tableFilename << " not found." << std::endl;
        exit(0);
    }

    // Intialize confusion matrix
    int metadataSize = metadata.classes.size();
    int confusionMatrix[metadataSize][metadataSize];
    for(size_t i = 0; i < metadataSize; i++)
    {
        for(size_t j = 0; j < metadataSize; j++)
        {
            confusionMatrix[i][j] = 0;
        }
    }

    int counter = 0;
    while(getline(file, str))
    {        
        counter++;
        // Get which class it belongs to
        int decisionStart = str.find_last_of(",") + 1;
        int lastChar = 1 + str.find_last_not_of(". \t\f\v\n\r");
        std::string decision = str.substr(decisionStart, lastChar - decisionStart);
        int decisionIndex = 0;
        for(size_t i = 0; i < metadata.classes.size(); i++)
        {
            if(decision == metadata.classes[i])
            {
                decisionIndex = i;
                break;
            }
        }

        std::vector<std::string> inputs;

        int startPos = 0;
        int endPos = 0;
        for(size_t i = 0; i < table.size(); i++)
        {
            endPos = str.find_first_of(",", startPos);
            std::string valueStr = str.substr(startPos, endPos - startPos);
            startPos = endPos + 1; // skip the ','

            // If this is a continuous value simply add it the vector and determine quadrant later
            if(table[i].isContinuous)
            {
                inputs.push_back(valueStr);
            }else
            {
                // Find matching 
                for(size_t j = 0; j < table[i].size(); j++)
                {
                    // If you find a matching conditonal variable attribute then increment the cooresponding class counter
                    if(table[i][j].name == valueStr)
                    {
                        inputs.push_back(valueStr);
                        break;
                    }
                    if(j == table[i].size() - 1)
                    {
                        std::cout << "No value [" << valueStr << "] in [" << table[i].name;                       
                        std::cout << "(";
                        for(size_t k = 0; k < table[i].size(); k++)
                        {
                            std::cout << table[i][k].name << ",";
                        }
                        std::cout << ")";
                        std::cout << "]. on line=" << counter << std::endl;
                        exit(0);
                    }
                }
            }
        }

        // Now determin probablity for each class using given values
        double probability[metadata.classes.size()];
        for(size_t i = 0; i < metadata.classes.size(); i++)
        {
            probability[i] = 1;
        }
        
        for(size_t i = 0; i < table.size(); i++)
        {
            if(table[i].isContinuous)
            {
                double value = atof(inputs[i].c_str());
                for(size_t j = 0; j < metadata.classes.size(); j++)
                {
                    probability[j] *= table[i].getProbabilityDenisty(value, j);
                }
            }else
            {
                // Find cooresponding index to input
                int index = 0;
                for(size_t j = 0; j < metadata.conditions[i]->values.size(); j++)
                {
                    if(inputs[i] == metadata.conditions[i]->values[j])
                    {
                        index = j;
                    }
                }

                int classCountSum = 0;
                for(size_t m = 0; m < table[i][index].size(); m++)
                {
                    classCountSum += table[i][index][m];
                }

                if(classCountSum == 0)
                    continue;

                for(size_t k = 0; k < metadata.classes.size(); k++)
                {
                    probability[k] *= (double) table[i][index][k] / (double) classCountSum;
                }
            }
        }

        // Now multiply the probability by the ratio of items a class had compared to the total
        for(size_t i = 0; i < metadata.classes.size(); i++)
        {
            probability[i] *= (double) table.itemsPerClass->at(i) / (double) table.totalItems;
        }

        // Find the highest probabilty
        double maxValue = -1;
        int predictedIndex = 0;
        for(size_t i = 0; i < metadata.classes.size(); i++){
            if(probability[i] > maxValue)
            {
                maxValue = probability[i];
                predictedIndex = i;
            }
        }
        confusionMatrix[decisionIndex][predictedIndex]++;
    }

    // Print the confusion matrix

    // Find class with max length
    int maxLength = 0;
    for(size_t i = 0; i < metadataSize; i++)
    {
        int length = metadata.classes[i].length();
        if(maxLength < length)
            maxLength = length;
    }

    std::cout << std::endl << "Confusion Matrix:" << std::endl << std::endl;
    std::cout << std::string(maxLength + 1, ' ');
    for(size_t i = 0; i < metadataSize; i++)
    {
        std::cout << metadata.classes[i] << " ";
    }
    std::cout << std::endl;
    std::cout << std::left;
    for(size_t i = 0; i < metadataSize; i++)
    {
        std::cout << metadata.classes[i] << std::string(maxLength - metadata.classes[i].length() + 1, ' ');
        for(size_t j = 0; j < metadataSize; j++)
        {
            std::cout << std::setw(metadata.classes[j].length());
            std::cout << confusionMatrix[i][j] << " ";
        }
        std::cout << std::endl;
    }

    file.close();

    return;
}

// Menu Item 3.1
void NaiveBayes::enterNewCases()
{
    if(table.empty())
    {
        std::cout << "You must have a table loaded to enter a new case in." << std::endl;
        std::cout << "Use menu item 1 or 5 to load a table in." << std::endl;
        return;
    }

    std::vector<std::string> inputs;

    std::cout << "Enter a case to get probablity of each class" << std::endl;
    
    for(size_t i = 0; i < metadata.conditions.size(); i++)
    {
        while(true)
        {
            std::cout << metadata.conditions[i]->name << ": ";
            std::cout << "possible inputs (";
            for(size_t j = 0; j < metadata.conditions[i]->values.size(); j++)
            {
                std::cout << "\'" << metadata.conditions[i]->values[j] << "\' ";
            }
            if(table[i].isContinuous)
            {
                
                for(size_t k = 0; k < metadata.classes.size(); k++)
                {
                    std::cout << "\'" << metadata.classes[k] << "\':";
                    std::cout << "mean=" << table[i][MEAN_INDEX][k];
                    std::cout << " std-dev=" << table[i][STD_DEV_INDEX][k];
                    std::cout << ", ";
                }
            }
            std::cout << ")" << std::endl << ":";

            std::string str;
            std::cin >> str;
            std::cout << std::endl;

            bool match = false;
            if(table[i].isContinuous)
            {   
                // currently no verification...
                inputs.push_back(str);
                break;
            }else
            {
                for(size_t j = 0; j < metadata.conditions[i]->values.size(); j++)
                {
                    if(str == metadata.conditions[i]->values[j])
                    {
                        match = true;
                        break;
                    }
                    
                }
                if(match){
                    inputs.push_back(str);
                    break;
                }
            }
        }    
    }

    // Now determin probablity for each class using given values
    double probability[metadata.classes.size()];
    for(size_t i = 0; i < metadata.classes.size(); i++)
    {
        probability[i] = 1;
    }
    
    for(size_t i = 0; i < table.size(); i++)
    {
        // Find cooresponding index to input
        int index = 0;
        if(table[i].isContinuous)
        {
            double value = atof(inputs[i].c_str());
            for(size_t j = 0; j < metadata.classes.size(); j++)
            {
                probability[j] = table[i].getProbabilityDenisty(value, j);
            }
        }else
        {
            for(size_t j = 0; j < metadata.conditions[i]->values.size(); j++)
            {
                if(inputs[i] == metadata.conditions[i]->values[j])
                {
                    index = j;
                }
            }

            int classCountSum = 0;
            for(size_t m = 0; m < table[i][index].size(); m++)
            {
                classCountSum += table[i][index][m];
            }

            if(classCountSum == 0)
                continue;

            for(size_t k = 0; k < metadata.classes.size(); k++)
            {
                // std::cout << i << "," << k << "," << table[i][index][k] << " = "  << std::endl;
                probability[k] *= (double) table[i][index][k] / (double) classCountSum;
            }
        }
    }

    // Now multiply the probability by the ratio of items a class had compared to the total
    for(size_t i = 0; i < metadata.classes.size(); i++)
    {
        probability[i] *= (double) table.itemsPerClass->at(i) / (double) table.totalItems;
    }

    int winnerIndex = 0;
    double winnerValue = -1;
    for(size_t i = 0; i < metadata.classes.size(); i++)
    {
        std::cout << "P(X|" << metadata.classes[i] << ") = " << probability[i] << std::endl;
        if(probability[i] > winnerValue)
        {
            winnerIndex = i;
            winnerValue = probability[i];
        }
    }

    std::cout << "The decision is '" << metadata.classes[winnerIndex] << "'." << std::endl;

    return;
}

// Menu Item 4
void NaiveBayes::saveClassifer()
{
    std::string tableFilename = DATA_DIRECTORY;
    std::string str;
    std::cout << "Name for table file (ex: vote.table): ";
    std::cin >> str;
    tableFilename += str;

    std::ofstream file;
    file.open(tableFilename.c_str(), std::ios_base::out);

    file << table.totalItems << " ";
    for(size_t i = 0; i < metadata.classes.size(); i++)
    {
        file << table.itemsPerClass->at(i);
        if(i != metadata.classes.size() - 1)
            file << " ";
    }
    file << std::endl;

    for(size_t i = 0; i < table.size(); i++)
    {
        file << table[i].name << ": ";

        if(table[i].isContinuous)
            file << "c ";
        else
            file << "n ";

        for(size_t j = 0; j < table[i].size(); j++)
        {
           file << table[i][j].name << "[";
            for(size_t k = 0; k < table[i][j].size(); k++)
            {
                file << table[i][j][k];
                if(k != table[i][j].size() - 1)
                    file << ",";
            }
           file << "]";
        }
        file << std::endl;
    }
    std::cout << "Saved." << std::endl;

    file.close();
}

// Menu Item 5
void NaiveBayes::loadClassifer()
{
    std::string metaFilename = DATA_DIRECTORY;
    std::string str;
    std::cout << "Name for metadata file (ex: vote.names): ";
    std::cin >> str;
    metaFilename += str;
    std::string tableFilename = DATA_DIRECTORY;
    std::cout << "Name for table file (ex: vote.table): ";
    std::cin >> str;
    tableFilename += str;


    parseMetaData(metaFilename);

    std::ifstream file;
    file.open(tableFilename.c_str(), std::ios_base::app);

    if(file.fail())
    {
        std::cout << tableFilename << " not found." << std::endl;
        exit(0);
    }

    // Make sure we have an empty table
    if(!table.empty())
        table.clear();

    // Make sure we only have one 'itemsPerClass' and intilize a new one
    if(table.itemsPerClass)
        delete table.itemsPerClass;

    table.initializeClassCounter(metadata.classes.size());

    // Parse first line about class counts from data file
    getline(file,str);
    int startPos = 0;
    int endPos = 0;
    endPos = str.find_first_of(" ", startPos);
    table.totalItems = atoi(str.substr(startPos, endPos).c_str());
    int k = 0;
    // For every value after the first parse them and put them into the 'itemsPerClass' vector
    while(endPos != std::string::npos)
    {
        startPos = endPos + 1; // skip space
        endPos = str.find_first_of(" ", startPos);
        int value = atoi(str.substr(startPos,endPos - startPos).c_str());
        table.itemsPerClass->at(k) = value;
        k++;
    }

    


    int i = 0;
    while(getline(file,str))
    {
        startPos = 0;
        endPos = 0;

        // Get class info
        endPos = str.find_first_of(":", startPos);
        // Skip empty lines
        if(endPos == std::string::npos)
            continue;
        table.push_back(Attribute());
        table[i].name = str.substr(startPos, endPos - startPos);

        startPos = str.find_first_not_of(" \t\f\v\n\r", endPos + 1);
        if(str.substr(startPos, 1) == "c"){
            table[i].isContinuous = true;
        }else
            table[i].isContinuous = false;

        startPos = str.find_first_not_of(" \t\f\v\n\r", startPos + 1);
    
        // Get each condition variable
        endPos = str.find_first_of("[", startPos);
        int attributeEndPos = str.find_first_of("]", endPos);
        int j = 0;
        while(endPos != std::string::npos)
        {
            std::string name = str.substr(startPos, endPos - startPos);
            std::vector<double> counters;
            while(endPos + 1 < attributeEndPos)
            {
                startPos = endPos + 1;
                endPos = str.find_first_of(",]", startPos);
                float value = atof(str.substr(startPos,endPos - startPos).c_str());
                counters.push_back(value);
            }

            table[i].push_back(ClassCounter(counters.size()));
            table[i][j].name = name;
            for(size_t k = 0; k < counters.size(); k++)
            {
                table[i][j][k] = counters[k];
            }
            
            startPos = 1 + str.find_first_of("]", endPos);
            endPos = str.find_first_of("[", startPos);
            attributeEndPos = str.find_first_of("]", endPos);
            j++;
        }
        i++;
    }
    std::cout << "Loaded." << std::endl;
}


void NaiveBayes::initialzeTableFromMetaData()
{
    // For every condition variable inside the metdata
    int classCount = metadata.classes.size();
    for(size_t i = 0; i < metadata.conditions.size(); i++)
    {
        table.push_back(Attribute());
        table[i].name = metadata.conditions[i]->name;

        // If the condition variable is continuous
        if(metadata.conditions[i]->values[0] == "continuous")
        {
            table[i].isContinuous = true;
            // Push quadrants onto the vector for continuous variables
            for(size_t j = 0; j < QUADRANT_COUNT; j++)
            {
                table[i].push_back(ClassCounter(classCount));
                // Push a counter for each class in the metadata
            }  
        }else
        {
            // For every value accociated with that condition
            for(size_t j = 0; j < metadata.conditions[i]->values.size(); j++)
            {
                table[i].push_back(ClassCounter(classCount));
                table[i][j].name = metadata.conditions[i]->values[j];
                // Push a counter for each class in the metadata
            } 
        }
    }
}

void NaiveBayes::parseDataFile(std::string filename)
{
    std::ifstream file;
    file.open(filename.c_str(), std::ios_base::app);

    if(file.fail())
    {
        std::cout << filename << " not found." << std::endl;
        exit(0);
    }

    std::vector<std::vector<ValueClassPair>> contValues;
    // Push an empty vector for every condition vairable (even if they're not continuous)
    for(size_t i = 0; i < table.size(); i++)
    {
        contValues.push_back(std::vector<ValueClassPair>());
    }
    
    // Make sure you only have one 'itemsPerClass' inside the table
    if(table.itemsPerClass)
        delete table.itemsPerClass;

    table.initializeClassCounter(metadata.classes.size());
    
    // Parse Data
    std::string str;
    int lineCount = 0;
    while(getline(file, str))
    {        
        lineCount++;
        // Get which class it belongs to
        int decisionStart = str.find_last_of(",") + 1;
        int lastChar = 1 + str.find_last_not_of(". \t\f\v\n\r");
        std::string decision = str.substr(decisionStart, lastChar - decisionStart);
        int decisionIndex = 0;
        for(size_t i = 0; i < metadata.classes.size(); i++)
        {
            if(decision == metadata.classes[i])
            {
                decisionIndex = i;
                break;
            }
        }
        table.itemsPerClass->at(decisionIndex)++;

        int startPos = 0;
        int endPos = 0;
        for(size_t i = 0; i < table.size(); i++)
        {
            endPos = str.find_first_of(",", startPos);
            std::string valueStr = str.substr(startPos, endPos - startPos);
            startPos = endPos + 1; // skip the ','

            // If this is a continuous value simply add it the vector and determine quadrant later
            if(table[i].isContinuous)
            {
                double value = std::atof(valueStr.c_str());
                contValues[i].push_back(std::pair<double, int>(value, decisionIndex));
            }else
            {
                // Find matching 
                for(size_t j = 0; j < table[i].size(); j++)
                {
                    // If you find a matching conditonal variable attribute then increment the cooresponding class counter
                    if(table[i][j].name == valueStr)
                    {
                        table[i][j][decisionIndex]++;
                        break;
                    }
                    if(j == table[i].size() - 1)
                    {
                        std::cout << "No value [" << valueStr << "] in [" << table[i].name;                       
                        std::cout << "(";
                        for(size_t k = 0; k < table[i].size(); k++)
                        {
                            std::cout << table[i][k].name << ",";
                        }
                        std::cout << ")";
                        std::cout << "]. on line=" << lineCount << std::endl;
                        exit(0);
                    }
                }
            }
        }
    }

    table.totalItems = lineCount;
    

    for(size_t i = 0; i < table.size(); i++)
    {
        if(table[i].isContinuous)
        {
            table[i].setDistributionValues(contValues[i], metadata.classes.size());
        }
    }
}

void NaiveBayes::printTable()
{
    std::cout << "Table:" << std::endl;
    std::cout << " Total Data Items: " << table.totalItems << ",  ";
    
    for(size_t i = 0; i < metadata.classes.size(); i++)
    {
        std::cout << "\'" << metadata.classes[i] << "\' items: " << table.itemsPerClass->at(i) << " ";
    }
    std::cout << std::endl;

    for(size_t i = 0; i < table.size(); i++)
    {
        std::cout << "  " << table[i].name << ": ";
        for(size_t j = 0; j < table[i].size(); j++)
        {
           std::cout << table[i][j].name << "[";
            for(size_t k = 0; k < table[i][j].size(); k++)
            {
                std::cout << table[i][j][k] << ",";
            }
           std::cout << "] ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}


// Parse the metadata off of fileStem.names
void NaiveBayes::parseMetaData(std::string filename)
{
    if(!metadata.classes.empty())
    {
        metadata.classes.clear();
    }
    if(!metadata.conditions.empty())
    {
        metadata.conditions.clear();
    }
    
    std::ifstream file;
    file.open(filename.c_str(), std::ios_base::app);

    if(file.fail())
    {
        std::cout << filename << " not found." << std::endl;
        exit(0);
    }

    // Skip all comments and blank lines
    std::string str;
    while(getline(file, str))
    {
        std::string temp = str;
        temp.erase(remove_if(temp.begin(), temp.end(), isspace), temp.end());
        if(temp[0] != '|' && temp.length() != 0)
        {
            break;
        }
    }

    // Parse Classes

    // Remove trailing spaces and any comments
    int start = 0;
    int commentPos = str.find_first_of('|');
    if(commentPos != std::string::npos)
    {
        str = str.substr(start, commentPos);
    }

    int lastChar = str.find_last_not_of(",. \t\f\v\n\r");
    if(lastChar == str.length() - 1)
    {
        str = str + '.';
    }
    else if(str[lastChar + 1] != '.')
    {
        str[lastChar + 1] = '.';
    }

    int end = str.find_first_of(",.", start);
    while(end != std::string::npos)
    {
        // remove leading spaces
        int nonWhiteStart = str.find_first_not_of(" \t\f\v\n\r", start);
        metadata.classes.push_back(str.substr(nonWhiteStart, end - nonWhiteStart));
        start = end + 1; // 1 == size of delimiter
        end = str.find_first_of(".,", start);
    }

    // Skip all comments and blank lines
    while(getline(file, str))
    {
        std::string temp = str;
        temp.erase(remove_if(temp.begin(), temp.end(), isspace), temp.end());
        if(temp[0] != '|' && temp.length() != 0)
        {
            break;
        }
    }
    
    // Parse Conditions
    do
    {
        ConditionVariable* var = new ConditionVariable();

        // remove  trailing spaces and any comments
        int start = 0;
        int commentPos = str.find_first_of('|');
        if(commentPos != std::string::npos)
        {
            str = str.substr(start, commentPos);
        }

        int lastChar = str.find_last_not_of(",. \t\f\v\n\r");
        if(lastChar == str.length() - 1)
        {
            str = str + '.';
        }
        else if(str[lastChar + 1] != '.')
        {
            str[lastChar + 1] = '.';
        }
        
        // get attribute name
        int end = str.find_first_of(":");
        var->name = str.substr(start, end);

        // Parse condtion variables
        start = end + 1; // skip the ':'
        end = str.find_first_of(",.", start);
        while(end != std::string::npos)
        {
            // remove leading spaces
            int nonWhiteStart = str.find_first_not_of(" \t\f\v\n\r", start);
            var->values.push_back(str.substr(nonWhiteStart, end - nonWhiteStart));
            start = end + 1; // skip delimiter
            end = str.find_first_of(".,", start);
        }

        metadata.conditions.push_back(var);
    }while(getline(file, str));

    file.close();
}

// Print Metadata
void NaiveBayes::PrintMetaData()
{
    std::cout << "Printing metadata..." << std::endl;
    std::cout << "  classes: ";
    for(size_t i = 0; i < metadata.classes.size(); i++)
    {
        std::cout << '\'' << metadata.classes.at(i) << '\'' << ", ";
    }
    std::cout << std::endl;

    for(size_t i = 0; i < metadata.conditions.size(); i++)
    {
        std::cout << metadata.conditions[i]->name << ": ";
        std::vector<std::string>* curr_var = &metadata.conditions[i]->values;
        for(size_t j = 0; j < curr_var->size(); j++)
        {
            std::cout << '\'' << curr_var->at(j) << '\'' << ", ";
        }
        std::cout << std::endl;
    }
    std::cout << "Done. " << std::endl;
}