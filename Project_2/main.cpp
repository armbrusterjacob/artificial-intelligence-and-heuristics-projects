#include <iomanip>
#include <iostream>
#include <fstream>
#include <limits>
#include "naiveBayes.hpp"

void menu();
void submenu(NaiveBayes*);

int main(int argc, char* argv[])
{
    menu();
    return 0;
}

void menu()
{
    const int QUIT = 6;
    int choice = -1, subChoice = -1;

    NaiveBayes* bayes = new NaiveBayes();

    while(choice != QUIT)
    {
        std::cout << "1. Learn naïve Bayesian classifer." << std::endl;
        std::cout << "2. Testing accuracy of the classifer." << std::endl;
        std::cout << "3. Applying the classifer to new cases." << std::endl;
        std::cout << "4. Save the classifer to external file." << std::endl;
        std::cout << "5. Load a classifer and apply it to new cases interactively as in menu 3." << std::endl;
        std::cout << "6. Quit." << std::endl;

        std::cout << "Enter your choice: ";
        std::cin >> choice;

        if(std::cin.fail())
        {
            std::cout << std::endl << "Input is not a number." << std::endl << std::endl;
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            continue;
        }

        
        switch(choice)
        {
            case 1:
                bayes->learnNaiveBayes();
                break;
            case 2:
                bayes->testAccuracy();
                break;
            case 3:
                submenu(bayes);
                break;
            case 4:
                bayes->saveClassifer();
                break;
            case 5:
                bayes->loadClassifer();
                break;
            case QUIT:
                break;

            default:
                std::cout << "Invalid menu choice." << std::endl;
                break;
        }

        std::cout << std::endl;
    }
}

void submenu(NaiveBayes* bayes)
{
    const int QUIT = 3;
    int choice = -1, subChoice = -1;

    while(choice != QUIT)
    {
        std::cout << "    3.1. Apply to a new case." << std::endl;
        std::cout << "    3.2. Print Classifer Table." << std::endl;
        std::cout << "    3.3. Quit." << std::endl;

        std::cout << "Enter your choice: ";
        std::cin >> choice;

        switch(choice)
        {
            case 1:
                bayes->enterNewCases();
                break;
            case 2:
                bayes->printTable();
                break;
            case QUIT:
                return;
                break;
        }
    }
}