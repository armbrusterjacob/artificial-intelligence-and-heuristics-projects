Class: Artifical Inteligence and Heuristic Programming
Name: Jacob Armbruster

In order to use this program you need to use cmake inside the build folder as shown:

cmake ..
make

This will create naiveBayes which can be then be run with "./naiveBayes" in the build folder.

THE FILES FOLDER IS REQUIRED TO BE IN THE SAME DIRECTORY AS THE PROGRAM. This is because
it contains the c4.5 format files needed for input.

This program is a naive bayesian classifer, a program used to classify decision values given inputs.
Theoretically it could use any table that has continuous and non-continuous values for it's attributes,
however it needs to be in the c4.5 format for now.

The c4.5 format files are in the form '.names' for metadata, '.data' for training data,
'.test' for testing data, and '.table' which is unique to this program and stores the
baysian classifier as a table.

3 sets of examples are inside the "files/" directory.
* crx, anonymous credit card data
* golf, similar to the "tennis example"
    - I created the test file myself by changing values inside the data file around.
      so the confusion matrix isn't very good looking.
* vote, voting data about certain issues to determine democrat/republican.