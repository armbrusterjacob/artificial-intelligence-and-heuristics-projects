set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

project (naiveBayes)

add_executable(naiveBayes main.cpp)

add_library(lib
	naiveBayes.cpp
	naiveBayes.hpp
)

target_link_libraries(naiveBayes lib)

