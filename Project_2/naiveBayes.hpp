#pragma once

#include <cstring>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <stack>
#include <cfloat>
#include <random>

const std::string DATA_DIRECTORY = "files/";
const int COUNT_START= 0;
const int QUADRANT_COUNT = 4;

enum Quadrant
{
    first = 0,
    second,
    third,
    fourth,
};

struct ConditionVariable
{
    std::string name;
    std::vector<std::string> values;
};

struct MetaData
{
    std::vector<std::string> classes;
    std::vector<ConditionVariable*> conditions;
};

struct ClassCounter : std::vector<double>
{
    std::string name;

    ClassCounter(int classCount)
    {
        for(size_t i = 0; i < classCount; i++)
        {
            push_back(0);
        }
    }
};

using ValueClassPair = std::pair<double, int>;

const int MEAN_INDEX = 0;
const int STD_DEV_INDEX = 1;

struct Attribute : std::vector<ClassCounter> 
{
    std::string name;
    bool isContinuous = false;
    // for continuous variables
    std::normal_distribution<>* distribution;

    void setDistributionValues(const std::vector<std::pair<double, int>> &continuousValues, int classCount)
    {
        //should only be applied to continuous attributes, so there shouldn't be any condition variables listed
        if(!isContinuous)
        {
            std::cerr << "Non-Continuous attribute attempting to create distribution, exiting..." << std::endl;
            exit(0);
        }
        // make sure there aren't any condition values listed
        if(!empty())
            clear();

        // Add class counts for the means and standard deviations
        push_back(ClassCounter(classCount));
        at(MEAN_INDEX).name = "m";
        push_back(ClassCounter(classCount));
        at(STD_DEV_INDEX).name = "sd";
        
        // get the count for each class in the vector
        int classSize[classCount];
        for(size_t i = 0; i < classCount; i++)
        {
            classSize[i] = 0;
        }
        for(size_t i = 0; i < continuousValues.size(); i++)
        {
            classSize[continuousValues[i].second]++;
        }
        
        for(size_t i = 0; i < classCount; i++)
        {
            double meanSum = 0;
            for(size_t j = 0; j < continuousValues.size(); j++)
            {
                if(continuousValues[j].second == i)
                    meanSum += continuousValues[j].first;
            }
            double mean = meanSum / classSize[i];

            double stdDevSum = 0;
            for(size_t j = 0; j < continuousValues.size(); j++)
            {
                if(continuousValues[j].second == i)
                    stdDevSum += std::pow(continuousValues[j].first - mean, 2.0);
            }
            double stdDev = std::sqrt(stdDevSum / (double) classSize[i]);
            // std::cout << i  << ", " << classSize[i] << ",: " << mean << ", " << meanSum << ",: " << stdDev << ", " << stdDevSum << std::endl;

            at(MEAN_INDEX)[i] = mean;
            at(STD_DEV_INDEX)[i] = stdDev;
        }
        
        return;
    }

    double getProbabilityDenisty(double value, int classIndex)
    {
        double mean = at(MEAN_INDEX)[classIndex];
        double stdDev = at(STD_DEV_INDEX)[classIndex];
        double exponent = -0.5 * std::pow((value - mean) / stdDev, 2.0);
        double probDensity = (1.0 / (stdDev * std::sqrt(2.0 * M_PI)));
        probDensity *= std::pow(M_E, exponent);
        return probDensity;
    }
};

struct Table : std::vector<Attribute>
{
    ClassCounter* itemsPerClass;
    int totalItems = 0;

    void initializeClassCounter(int classCount)
    {
        itemsPerClass = new ClassCounter(classCount);
    }
};

class NaiveBayes
{
    MetaData metadata;
    Table table;

    public: 
    void learnNaiveBayes();
    void testAccuracy();
    void enterNewCases();
    void saveClassifer();
    void loadClassifer();
    void initialzeTableFromMetaData();
    void parseDataFile(std::string filename);
    void printTable();
    void parseMetaData(std::string filename);
    void PrintMetaData();
};